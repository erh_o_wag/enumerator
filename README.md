# Enumerator #

_Enumerator_ is a free extension for [Visual Studio Code](https://code.visualstudio.com/), designed to reduce repetitive work by extending the [multiple selections](https://code.visualstudio.com/docs/editor/editingevolved#_selection-multicursor) feature of [Visual Studio Code](https://code.visualstudio.com/).
This work is inspired by the features of the Sublime Text Plugin [Text Pastry](https://github.com/duydao/Text-Pastry)

See the [changelog](CHANGELOG.md) for changes and roadmap.

## Features include: ##

- Incremental numbers/sequences _(1, 2, 3 or 100, 80, 60)_
- Replicating Lines with a sequence
- Generate UUIDS (lower case: 3416721f-4171-422a-be23-8525c924226f or upper case:3890F852-D859-4FE2-9EC7-24A6B1522908)
- Create and modify selections

## How to use it
Use the option Enumerate in the command pallete or press Ctrl + Alt + e or Cmd + Option + e for Mac.

### AutoStep ###
Command: ```as <text> [step] [x-arg]```
```
    as row-1
```

For each selection we've made, _Enumerator_ will insert the text and increment the number by ``[step]`` (default is ``1``). As example, if we had 4 selections, we will get this:

Source:
```
    <div id="|"></div>
    <div id="|"></div>
    <div id="|"></div>
    <div id="|"></div>
```

Result:
```
    <div id="row-1"></div>
    <div id="row-2"></div>
    <div id="row-3"></div>
    <div id="row-4"></div>
```

Auto step supports a step size argument:
```
    as row-0 2
```

Result:
```
    <div id="row-0"></div>
    <div id="row-2"></div>
    <div id="row-4"></div>
    <div id="row-6"></div>
```

Auto Step also supports spaces in the text, and the start number embedded in the text

```
    as step 0: do this  2
```

Source:
```
    |
    |
    |
    |
```

Result:
```
    step 0: do this
    step 2: do this
    step 4: do this
    step 6: do this
```

### Repeat argument (x-arg) ###
```
    as 1 x5
```

Will result in :
```
    1|
    2|
    3|
    4|
    5|
```

_Enumerator_ will duplicate the line and add the number sequence to it, so the line doesn't have to be empty:

Source:
```
    <div id="row-|"></div>
```

Using ``as 1 x3`` will give us this:

```
    <div id="row-1"></div>
    <div id="row-2"></div>
    <div id="row-3"></div>
```

Auto Step supports the x-arg:

Source:

```
    <div id="|"></div>
```

Command:

```
    as row-10 10 x5
```

will expand to this:
```
    <div id="row-10"></div>
    <div id="row-20"></div>
    <div id="row-30"></div>
    <div id="row-40"></div>
    <div id="row-50"></div>
```

### UUIDs ###
Source:
```
    |
    |
    |
```

Command:
```
    uuid
```

Result:
```
    23b34b81-f8c6-463a-9cd2-777304925c0c
    55b22f28-c36f-419d-975f-0e1680497575
    68327724-4600-4ae6-8d76-66bbbdbe2e4c
```

Command:
```
    UUID
```

Result:
```
    147A7ACF-FBF6-4EE0-8F15-F4D3042C2CD6
    B284A6F3-005E-410B-B24A-688DCC46FA9B
    37753DDE-916E-47F8-8CB1-B70BDA1D88EC
```

UUID/uuid supports the x-arg:

Source:

```
    <div id="|"></div>
```

Command:

```
    uuid x5
```

will expand to this:
```
    <div id="f9b9cf61-ca8b-40c4-8cdc-4e9aa64b6626"></div>
    <div id="9988299c-d39c-4df1-9f26-cd146c13bbb8"></div>
    <div id="688caf2b-5d28-493c-b6ae-89a346f02a33"></div>
    <div id="cf575e18-b4f4-4ef9-9475-c1d476de24a8"></div>
    <div id="b0c3fd78-1d70-41f0-add1-6652a763c84d"></div>
```