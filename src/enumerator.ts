'use strict';

import * as vscode from 'vscode';
let uuid = require('node-uuid');

export function autoStep(textEditor: vscode.TextEditor, cmdParams: string){
    /*interface RegExp {
        $1: string; //Group 1: text before number to repeat 
        $2: string; //Group 2: start number
        $3: string; //Group 3: text after number to repeat
        $4: string; //Group 4: step size           
        $5: string; //Group 5: xarg - line replication           
    }*/
    let commandParser = new RegExp('(.*?)(\\d+)(?:([^\\d]*)(?= x?\\d))?(?:\\s+(-?\\d+))?(?:\\s+x(\\d+))?');
    let patternPieces = commandParser.exec(cmdParams);
    if (!patternPieces) {
        vscode.window.showInformationMessage("command pattern is invalid. Remember to include a number in repeat text and xarg comes after steps");
        return;
    }
    
    let repeatTextBeforeNumber = patternPieces[1] ? patternPieces[1] : "";
    let startNumber = parseInt(patternPieces[2]);
    let repeatTextAfterNumber = patternPieces[3] ? patternPieces[3] : "";
    let step = patternPieces[4] ? parseInt(patternPieces[4]) : 1;
    let xarg = patternPieces[5] ? parseInt(patternPieces[5]) : 0;
    let counter = startNumber;
    
    if (xarg == 0) {
        textEditor.edit(editBuilder => {
            textEditor.selections.forEach(element => {
                editBuilder.replace(element, repeatTextBeforeNumber + String(counter) + repeatTextAfterNumber);
                counter += step;
            });
        });
    } else if ( textEditor.selections.length == 1  && textEditor.selection.isSingleLine ) {
        textEditor.edit(editBuilder => {
            
            let activeSelection = textEditor.selection.active;
            let activeLine = textEditor.document.lineAt(activeSelection);
            let lineText = activeLine.text;
            let insertPos = activeSelection.character;
            let textBeforeCounter = lineText.substring(0, insertPos) + repeatTextBeforeNumber;
            let textAfterCounter = repeatTextAfterNumber + lineText.substring(insertPos, lineText.length);
            let generatedLines = textBeforeCounter + String(counter) + textAfterCounter;
            
            for (var linesRepeated = 1; linesRepeated < xarg; linesRepeated++) {
                counter += step;
                generatedLines += ( '\n' + textBeforeCounter + String(counter) + textAfterCounter);                                
            }
            editBuilder.replace(activeLine.range, generatedLines);
        });
    } else {
        vscode.window.showInformationMessage("xarg option only works with replicating a single line"); 
    }
}   

export function insertUuid(textEditor: vscode.TextEditor, cmdParams: string, isUpperCase: boolean){
    let uuidFunction = isUpperCase ? () => uuid.v4().toUpperCase() : () => uuid.v4();
    let xarg = (cmdParams.length && cmdParams[0].toLowerCase() == "x") ? parseInt(cmdParams.substring(1)) : 0;
    
    if (xarg == 0) {
        textEditor.edit(editBuilder => {
            textEditor.selections.forEach(element => {
                editBuilder.replace(element, uuidFunction());
            });
        });
    } else if ( textEditor.selections.length == 1  && textEditor.selection.isSingleLine ) {
        textEditor.edit(editBuilder => {
            
            let activeSelection = textEditor.selection.active;
            let activeLine = textEditor.document.lineAt(activeSelection);
            let lineText = activeLine.text;
            let insertPos = activeSelection.character;
            let textBeforeUUID = lineText.substring(0, insertPos);
            let textAfterUUID = lineText.substring(insertPos, lineText.length);
            let generatedLines = textBeforeUUID + String(uuidFunction()) + textAfterUUID;
            
            for (var linesRepeated = 1; linesRepeated < xarg; linesRepeated++) {
                generatedLines += ( '\n' + textBeforeUUID + String(uuidFunction()) + textAfterUUID);                                
            }
            editBuilder.replace(activeLine.range, generatedLines);
        });
    } else {
        vscode.window.showInformationMessage("xarg option only works with replicating a single line"); 
    }
}