# Roadmap
- [ ] Upper Case, lower case selection
- [ ] Add date insertion
- [ ] Pasting into selections from clipboard contents
 
# Changelog
These are the changes to each version that has been released
on the official Visual Studio extension gallery.

## 0.0.5
- [x] insert UUIDs at selection or with xarg
- [x] update keywords in extension description 

## 0.0.4
- [x] bugfix for decrementing sequences

## 0.0.3
- [x] support xarg for replicating lines

## 0.0.2
- [x] correct typo in Readme

## 0.0.1
- [x] Initial Release 
- [x] Insert sequences at selections